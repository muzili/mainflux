package cassandra

import (
	"github.com/gocql/gocql"
	"github.com/mainflux/mainflux/writer"
)

var _ writer.MessageRepository = (*msgRepository)(nil)

type msgRepository struct {
	session *gocql.Session
}

// NewMessageRepository instantiates Cassandra message repository.
func NewMessageRepository(session *gocql.Session) writer.MessageRepository {
	return &msgRepository{session}
}

func (repo *msgRepository) Save(raw writer.RawMessage) error {
	var (
		err  error
	)

	cql := `INSERT INTO messages_by_channel
		(channel, id, publisher, protocol, payload)
		VALUES (?, now(), ?, ?, ?)`

		err = repo.session.Query(cql, raw.Channel, raw.Publisher, raw.Protocol, raw.Payload).Exec()

		if err != nil {
			return err
		}

	return nil
}
