package manager

// Client represents a Mainflux client. Each client is owned by one user, and
// it is assigned with the unique identifier and (temporary) access key.
type Package struct {
	Id          string `json:"id,omitempty"`
	Name        string `json:"name"`
	VersionName string `json:"versionName,omitempty"`
	VersionCode int    `json:"versionCode"`
	Url         string `json:"url,omitempty"`
	Md5         string `json:"md5,omitempty"`
	Size        int    `json:"size,omitempty"`
	Description string `json:"desc,omitempty"`
}

type PackageList struct {
	Subject  string    `json:"subject,omitempty"`
	Uuid     string    `josn:"uuid,omitempty"`
	Appid    string    `json:"appid,omitempty"`
	Packages []Package `json:"apps"`
}

// Validate returns an error if client representation is invalid.
func (c *Package) Validate() error {
	return nil
}

// PackageRepository specifies a package persistence API.
type PackageRepository interface {
	// Id generates new resource identifier.
	Id() string
	// One retrieves the package having the provided identifier, that is owned
	// by the specified user.
	List(string, string, PackageList) (PackageList, error)

	Update(Package) error

	Save(Package) error

	Remove(string) error

	One(string, int) (Package, error)

	All(string) ([]Package, error)
}
