package api

import (
	"crypto/sha256"
	"fmt"
	"strings"

	"github.com/asaskevich/govalidator"
	"github.com/mainflux/mainflux/manager"
	"encoding/hex"
	"bytes"
)

func deviceValidate(id string) bool {
	if !govalidator.IsUUID(id) {
		fmt.Printf("Wrong uuid %s", id)
	}
	if len(id) != 36 {
		fmt.Errorf("invalid UUID length: %d", len(id))
		return false
	}
	packed_uuid := strings.Replace(id, "-", "", -1)
	//fmt.Printf("oid is %s\n", packed_uuid)
	uid := packed_uuid[0:16]
	//fmt.Printf("uid is %s\n", uid)
	chksum := packed_uuid[16:24]
	//fmt.Printf("sum is %s\n", chksum)
	h := sha256.New()
	h.Write([]byte(uid))
	sum := h.Sum(nil)[0:4]
	//fmt.Printf("new sum is %x\n", sum)
	decoded, err := hex.DecodeString(chksum)
	if err != nil {
		fmt.Errorf("Wrong checksum format %s", chksum)
		return false
	}
	if bytes.Compare(decoded, sum) != 0 {
		fmt.Errorf("Wrong checksum %x/%x\n", decoded, sum)
		return false
	}
	return true
}

type apiReq interface {
	validate() error
}

type userReq struct {
	user manager.User
}

func (req userReq) validate() error {
	return req.user.Validate()
}

type identityReq struct {
	key string
}

func (req identityReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	return nil
}

type addClientReq struct {
	key    string
	id     string
	client manager.Client
}

func (req addClientReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	if req.id == "" || !deviceValidate(req.id) {
		return manager.ErrUnauthorizedAccess
	}

	return req.client.Validate()
}

type updateClientReq struct {
	key    string
	id     string
	client manager.Client
}

func (req updateClientReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	if !deviceValidate(req.id) {
		return manager.ErrNotFound
	}

	return req.client.Validate()
}

type viewClientReq struct {
	key string
	id  string
}

func (req viewClientReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	if !deviceValidate(req.id) {
		return manager.ErrNotFound
	}

	return nil
}

type createChannelReq struct {
	key     string
	channel manager.Channel
}

func (req createChannelReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	return nil
}

type updateChannelReq struct {
	key     string
	id      string
	channel manager.Channel
}

func (req updateChannelReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	if !govalidator.IsUUID(req.id) {
		return manager.ErrNotFound
	}

	return nil
}

type viewResourceReq struct {
	key string
	id  string
}

func (req viewResourceReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	if !govalidator.IsUUID(req.id) {
		return manager.ErrNotFound
	}


	return nil
}

type listResourcesReq struct {
	key    string
	size   int
	offset int
}

func (req listResourcesReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	if req.size > 0 && req.offset >= 0 {
		return nil
	}

	return manager.ErrMalformedEntity
}

type listUpdateReq struct {
	key   string
	id    string
	plist manager.PackageList
}

func (req listUpdateReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	if req.id == "" {
		return manager.ErrNotFound
	}

	return nil
}

type addPackageReq struct {
	key string
	pkg manager.Package
}

func (req addPackageReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	return req.pkg.Validate()
}

type updatePackageReq struct {
	key string
	pkg manager.Package
}

func (req updatePackageReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	return req.pkg.Validate()
}

type viewPackageReq struct {
	key     string
	name    string
	version int
}

func (req viewPackageReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	return nil
}

type listPackageReq struct {
	key    string
	name   string
	size   int
	offset int
}

func (req listPackageReq) validate() error {
	if req.key == "" {
		return manager.ErrUnauthorizedAccess
	}

	return nil
}
