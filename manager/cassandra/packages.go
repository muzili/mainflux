package cassandra

import (
	"github.com/gocql/gocql"
	"github.com/mainflux/mainflux/manager"
	"fmt"
)

var _ manager.PackageRepository = (*packageRepository)(nil)

type packageRepository struct {
	session *gocql.Session
}

// NewPackageRepository instantiates Cassandra package repository.
func NewPackageRepository(session *gocql.Session) manager.PackageRepository {
	return &packageRepository{session}
}

func (repo *packageRepository) Id() string {
	return gocql.TimeUUID().String()
}

func (repo *packageRepository) List(key string, id string, plist manager.PackageList) (manager.PackageList, error) {
	subject := "update"
	packages := make([]manager.Package, 0)
	for i,pkg := range plist.Packages {
		cql := `SELECT id, version, url, md5, size, description from packages where name = ? and versionCode > ? ORDER BY versionCode DESC LIMIT 1`
		fmt.Printf("%d: cql:%s %s-%d\n", i, cql, pkg.Name, pkg.VersionCode)
		if err := repo.session.Query(cql, pkg.Name, pkg.VersionCode).
			Scan(&pkg.Id, &pkg.VersionName, &pkg.Url, &pkg.Md5, &pkg.Size,
			&pkg.Description); err != nil {
				continue
		}
		packages = append(packages, pkg)
	}
	ret := manager.PackageList{}
	ret.Subject = subject
	ret.Packages = packages
	return ret, nil
}

func (repo *packageRepository) Update(pkg manager.Package) error {
	cql := `UPDATE packages SET name = ?, version = ?, versionCode = ?,
            url = ?, md5 = ?, size = ?, description = ?
		    WHERE id = ? IF EXISTS`

	if _, err := repo.One(pkg.Name, pkg.VersionCode); err != nil {
		return fmt.Errorf("The package %s-%d not exists", pkg.Name, pkg.VersionCode)
	}

	applied, err := repo.session.Query(cql, pkg.Name, pkg.VersionName,
		pkg.VersionCode, pkg.Url, pkg.Md5, pkg.Size, pkg.Description, pkg.Id).ScanCAS()

	if !applied {
		return manager.ErrNotFound
	}

	return err
}

func (repo *packageRepository) Save(pkg manager.Package) error {

	cql := `INSERT INTO packages (id, name, version, versionCode, url, md5, size, description)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?)`

	if _, err := repo.One(pkg.Name, pkg.VersionCode); err == nil {
		return fmt.Errorf("The package %s-%d exists", pkg.Name, pkg.VersionCode)
	}

	if err := repo.session.Query(cql, pkg.Id, pkg.Name, pkg.VersionName,
		pkg.VersionCode, pkg.Url, pkg.Md5, pkg.Size, pkg.Description).Exec(); err != nil {
		return err
	}

	return nil
}

func (repo *packageRepository) Remove(id string) error {
	cql := `DELETE FROM packages WHERE id = ?`
	return repo.session.Query(cql, id).Exec()
}

func (repo *packageRepository) One(name string, versionCode int) (manager.Package, error) {
	cql := `SELECT id, version, url, md5, size, description
        FROM packages WHERE name = ? AND versionCode = ? LIMIT 1`

	pkg := manager.Package{
		Name:        name,
		VersionCode: versionCode,
	}

	if err := repo.session.Query(cql, name, versionCode).
		Scan(&pkg.Id, &pkg.VersionName, &pkg.Url, &pkg.Md5, &pkg.Size, &pkg.Description); err != nil {
		return pkg, manager.ErrNotFound
	}

	return pkg, nil
}

func (repo *packageRepository) All(pkg_name string) ([]manager.Package, error) {
	cql := `SELECT id, name, version, versionCode, url, md5, size, description from packages`
	if pkg_name != "" {
		cql = fmt.Sprintf("SELECT id, name, version, versionCode, url, md5, size, description from packages where name = '%s'", pkg_name)
	}
	var id string
	var name string
	var version string
	var versionCode int
	var url string
	var md5 string
	var size int
	var desc string

	fmt.Println(cql)
	// NOTE: the closing might failed
	iter := repo.session.Query(cql).Iter()

	pkgs := make([]manager.Package, 0)

	for iter.Scan(&id, &name, &version, &versionCode, &url, &md5, &size, &desc) {
		item := manager.Package{
			Id:          id,
			Name:        name,
			VersionName: version,
			VersionCode: versionCode,
			Url:         url,
			Md5:         md5,
			Size:        size,
			Description: desc,
		}

		fmt.Println(name + "-" + version)
		pkgs = append(pkgs, item)
	}
	if err := iter.Close(); err != nil {
		fmt.Println(err)
		return nil, err
	}

	return pkgs, nil
}
