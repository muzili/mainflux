package cassandra

import (
	"encoding/json"
	"fmt"

	"github.com/gocql/gocql"
	"github.com/mainflux/mainflux/updater"
	"github.com/mainflux/mainflux/writer"
	nats "github.com/nats-io/go-nats"
)

var _ updater.UpdaterRepository = (*updaterRepository)(nil)

type updaterRepository struct {
	session *gocql.Session
	nc      *nats.Conn
}

type clientMessage struct {
	Channel     string `json:"channel"`
	Publisher   string `json:"publisher"`
	Protocol    string `json:"protocol"`
	ContentType string `json:"content_type"`
	Payload     []byte `json:"payload"`
}
// NewUpdaterRepository instantiates Cassandra updater repository.
func NewUpdaterRepository(session *gocql.Session, nc *nats.Conn) updater.UpdaterRepository {
	return &updaterRepository{session, nc}
}

func (repo *updaterRepository) CheckUpdate(msg updater.CheckIn) error {
	var (
		publisher   string = "32e5e7ea-893b-4f92-a29c-9ce957453322"
		contentType string = "senml-json"
		topic       string = "msg.http"
		err         error
	)
	var body = `{"subject":"update"}`;
// 	var body = `{
//     "subject": "update",
//     "apps": [
//         {
//           "name":"com.whaley.installer",
//           "versionName":"1.1",
//           "versionCode":2,
//           "url":"http://default-1255596025.coscd.myqcloud.com/WhaleyInstaller.apk",
//           "md5":"de5316275c0eb8f84ab490e421fbf8a7",
//           "size":2444100,
//           "desc":""
//         },
//         {
//           "name":"com.whaley.launcher",
//           "versionName":"1.1.0",
//           "versionCode":2,
//           "url":"http://default-1255596025.coscd.myqcloud.com/WhaleyLauncher.apk",
//           "md5":"39e3f8bfe407a47ba6b1319117136647",
//           "size":9020799,
//           "desc":""
//         },
//         { "name":"com.helios.setting",
//           "versionName":"1.1.0",
//           "versionCode":2,
//           "url":"http://default-1255596025.coscd.myqcloud.com/WhaleySettings.apk",
//           "md5":"3abf82ac0fddbfa5d711efd26c2f937e",
//           "size":16288914,
//           "desc":""
//        },
//     ]
// }
// `
	raw := writer.RawMessage{
		Publisher:   publisher,
		ContentType: contentType,
		Channel:     msg.Publisher,
		Protocol:    "direct",
		Payload:     []byte(body),
	}

	b, err := json.Marshal(raw)
	if err != nil {
		//logger.Error("Failed to marshal checkin response", zap.Error(err))
		fmt.Printf("raw message is %s\n", raw)
		return err
	}

	fmt.Printf("Send check response: %s/%s\n", topic, b)

	repo.nc.Publish(topic, b)
	return nil
}

func (repo *updaterRepository) SavePackage(msg updater.Package) error {
	var (
		err error
	)

	cql := `INSERT INTO packages
		(pid, name, versionName, versionCode, md5, size, url, description)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?)`

	err = repo.session.Query(cql, msg.Pid, msg.Name, msg.VersionName, msg.VersionCode, msg.Md5, msg.Size, msg.Url, msg.Desc).Exec()

	if err != nil {
		return err
	}

	return nil
}

func (repo *updaterRepository) AddGroup(msg updater.Group) error {
	var (
		err error
	)

	cql := `INSERT INTO groups
		(gid, did, name, description)
		VALUES (?, ?, ?, ?)`

	err = repo.session.Query(cql, msg.Gid, msg.Did, msg.Name, msg.Desc).Exec()

	if err != nil {
		return err
	}

	return nil
}

func (repo *updaterRepository) Deploy(msg updater.Deployment) error {
	var (
		err error
	)

	cql := `INSERT INTO deployments
		(id, name, description, gid, pid)
		VALUES (?, ?, ?, ?, ?)`

	err = repo.session.Query(cql, msg.Id, msg.Name, msg.Desc, msg.Gid, msg.Pid).Exec()

	if err != nil {
		return err
	}

	return nil
}

func (repo *updaterRepository) Save(raw updater.RawMessage) error {
	var (
		err  error
	)

	cql := `INSERT INTO messages_by_channel
		(channel, id, publisher, protocol, payload)
		VALUES (?, now(), ?, ?, ?)`

		err = repo.session.Query(cql, raw.Channel, raw.Publisher, raw.Protocol, raw.Payload).Exec()

		if err != nil {
			return err
		}

	return nil
}
