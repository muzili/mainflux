package cassandra

import "github.com/gocql/gocql"

var tables []string = []string{
        `CREATE TABLE IF NOT EXISTS messages_by_channel (
                channel uuid,
                id timeuuid,
                publisher text,
                protocol text,
                payload text,
                PRIMARY KEY ((channel), id)
        ) WITH CLUSTERING ORDER BY (id DESC)`,
	`CREATE TABLE IF NOT EXISTS packages (
                pid timeuuid,
		name text,
                versionName text,
		versionCode int,
                md5    text,
                size   int,
                url    text,
                description   text,
		PRIMARY KEY ((name, versionCode), pid)
	) WITH CLUSTERING ORDER BY (pid DESC)`,
	`CREATE TABLE IF NOT EXISTS groups (
                gid uuid,
                did uuid,
		name text,
                description text,
		PRIMARY KEY (name, gid)
	) WITH CLUSTERING ORDER BY (gid DESC)`,

	`CREATE TABLE IF NOT EXISTS deployments (
                id timeuuid,
                name   text,
                description   text,
                gid    uuid,
                pid    timeuuid,
		PRIMARY KEY (name, id)
	) WITH CLUSTERING ORDER BY (id DESC)`,
}

// Connect establishes connection to the Cassandra cluster.
func Connect(hosts []string, keyspace string) (*gocql.Session, error) {
	cluster := gocql.NewCluster(hosts...)
	cluster.Keyspace = keyspace
	cluster.Consistency = gocql.Quorum

	return cluster.CreateSession()
}

// Initialize creates tables used by the service.
func Initialize(session *gocql.Session) error {
	for _, table := range tables {
		if err := session.Query(table).Exec(); err != nil {
			return err
		}
	}

	return nil
}
