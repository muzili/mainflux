package updater

type Apps struct {
	AppName    string `json:"an,omitempty"`
	AppVersion int    `json:"av,omitempty"`
}

type CheckIn struct {
	Channel   string `json:"-"`
	Publisher string `json:"-"`
	Protocol  string `json:"-"`
	Subject   string `json:"subject,omitempty"`
	Id        string `json:"uuid,omitempty"`
	AppId     string `json:"appid,omitempty"`
	Apps      []Apps `json:"apps,omitempty"`
}

type Package struct {
	Pid         string `json:"pid,omitempty"`
	Name        string `json:"name,omitempty"`
	VersionName string `json:"versionName,omitempty"`
	VersionCode string `json:"versionCode,omitempty"`
	Md5         string `json:"md5,omitempty"`
	Size        int    `json:"size,omitempty"`
	Url         string `json:"url,omitempty"`
	Desc        string `json:"desc,omitempty`
}

type Group struct {
	Gid  string `json:"gid,omitempty"`
	Name string `json:"name,omitempty"`
	Desc string `json:"desc,omitempty`
	Did  string `json:"did,omitempty"`
}

type Deployment struct {
	Id   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Desc string `json:"desc,omitempty`
	Gid  string `json:"gid,omitempty"`
	Pid  string `json:"pid,omitempty"`
}

type RawMessage struct {
	Channel     string `json:"channel"`
	Publisher   string `json:"publisher"`
	Protocol    string `json:"protocol"`
	ContentType string `json:"content_type"`
	Payload     []byte `json:"payload"`
}

// MessageRepository specifies a message persistence API.
type UpdaterRepository interface {
	// Save persists the message. A non-nil error is returned to indicate
	// operation failure.
	CheckUpdate(CheckIn) error
	SavePackage(Package) error
	AddGroup(Group) error
	Deploy(Deployment) error
	Save(RawMessage) error
}
