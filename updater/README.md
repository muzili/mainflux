# Updater

Updater consumes channel update request published on message broker,
and return the latest applications.

## Configuration

The service is configured using the environment variables presented in the
following table. Note that any unset variables will be replaced with their
default values.

| Variable            | Description                        | Default               |
|---------------------|------------------------------------|-----------------------|
| UPDATER_NATS_URL    | NATS instance URL                  | nats://localhost:4222 |
| UPDATER_DB_CLUSTER  | omma-separated Cassandra endpoints | 127.0.0.1             |
| UPDATER_DB_KEYSPACE | name of Cassandra keyspace         | updater                      |

## Deployment

```yaml
version: "2"
services:
  manager:
    image: muzili/mainflux-updater:[version]
    container_name: [instance name]
    environment:
      UPDATER_DB_CLUSTER: [comma-separated Cassandra endpoints]
      UPDATER_DB_KEYSPACE: [name of Cassandra keyspace]
      UPDATER_NATS_URL: [NATS instance URL]
```

