package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bitly/go-simplejson"
	"github.com/gocql/gocql"
	"github.com/mainflux/mainflux/updater"
	"github.com/mainflux/mainflux/updater/cassandra"
	nats "github.com/nats-io/go-nats"
	"go.uber.org/zap"
)

const (
	sep         string = ","
	topic       string = "msg.*"
	queue       string = "updater"
	defCluster  string = "127.0.0.1"
	defKeyspace string = "updater"
	defNatsURL  string = nats.DefaultURL
	envCluster  string = "UPDATER_DB_CLUSTER"
	envKeyspace string = "UPDATER_DB_KEYSPACE"
	envNatsURL  string = "UPDATER_NATS_URL"
)

var logger *zap.Logger

type config struct {
	Cluster  string
	Keyspace string
	NatsURL  string
}

func main() {
	cfg := loadConfig()

	logger, _ = zap.NewProduction()
	defer logger.Sync()

	session := connectToCassandra(cfg)
	defer session.Close()

	nc := connectToNats(cfg)
	defer nc.Close()

	repo := makeRepository(session, nc)

	nc.QueueSubscribe(topic, queue, func(m *nats.Msg) {
		msg := updater.RawMessage{}

		if err := json.Unmarshal(m.Data, &msg); err != nil {
			logger.Error("Failed to unmarshal raw message.", zap.Error(err))
			return
		}

		if err := repo.Save(msg); err != nil {
			logger.Error("Failed to save message.", zap.Error(err))
			return
		}

		js, err := simplejson.NewJson(msg.Payload)
		if err != nil {
			fmt.Printf("checkin msg: %s\n", msg.Payload)
			logger.Error("Failed to unmarshal subject", zap.Error(err))
			return
		}
		subject := js.Get("subject").MustString()
		if subject == "" {
			logger.Error("subject is nil")
			return
		}
		fmt.Println("subject is " + subject)
		if (subject == "checkin") {
			ci := updater.CheckIn{}
			ci.Publisher = msg.Publisher
			ci.Subject = subject
			ci.Id = msg.Publisher
			ci.AppId = js.Get("appid").MustString()
			ci.Channel = msg.Channel
			repo.CheckUpdate(ci)
		} else if (subject == "addPackage") {
			pack := updater.Package{}
			if err := json.Unmarshal([]byte(msg.Payload), &pack); err != nil {
				repo.SavePackage(pack)
			} else {
				logger.Error("Failed to unmarshal package message.", zap.Error(err))
			}
		} else if (subject == "addGroup") {
			group := updater.Group{}
			if err := json.Unmarshal([]byte(msg.Payload), &group); err != nil {
				repo.AddGroup(group)
			} else {
				logger.Error("Failed to unmarshal group message.", zap.Error(err))
			}
		} else if (subject == "deploy") {
			deploy := updater.Deployment{}
			if err := json.Unmarshal([]byte(msg.Payload), &deploy); err != nil {
				repo.Deploy(deploy)
			} else {
				logger.Error("Failed to unmarshal deploy message.", zap.Error(err))
			}
		} else {
			fmt.Printf("Unknown message %s\n", msg.Payload)
		}

	})

	forever()
}
func loadConfig() *config {
	return &config{
		Cluster:  env(envCluster, defCluster),
		Keyspace: env(envKeyspace, defKeyspace),
		NatsURL:  env(envNatsURL, defNatsURL),
	}
}

func env(key, fallback string) string {
	value := os.Getenv(key)
	if value == "" {
		return fallback
	}

	return value
}

func connectToCassandra(cfg *config) *gocql.Session {
	hosts := strings.Split(cfg.Cluster, sep)

	s, err := cassandra.Connect(hosts, cfg.Keyspace)
	if err != nil {
		logger.Error("Failed to connect to DB", zap.Error(err))
	}

	return s
}

func makeRepository(session *gocql.Session, nc *nats.Conn) updater.UpdaterRepository {
	if err := cassandra.Initialize(session); err != nil {
		logger.Error("Failed to initialize message repository.", zap.Error(err))
	}

	return cassandra.NewUpdaterRepository(session, nc)
}

func connectToNats(cfg *config) *nats.Conn {
	nc, err := nats.Connect(cfg.NatsURL)
	if err != nil {
		logger.Error("Failed to connect to NATS.", zap.Error(err))
	}

	return nc
}
func forever() {
	errs := make(chan error, 1)

	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	<-errs
}
